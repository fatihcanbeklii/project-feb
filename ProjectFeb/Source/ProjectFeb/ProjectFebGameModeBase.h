// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjectFebGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTFEB_API AProjectFebGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
