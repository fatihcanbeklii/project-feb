// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROJECTFEB_ProjectFebGameModeBase_generated_h
#error "ProjectFebGameModeBase.generated.h already included, missing '#pragma once' in ProjectFebGameModeBase.h"
#endif
#define PROJECTFEB_ProjectFebGameModeBase_generated_h

#define ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_SPARSE_DATA
#define ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_RPC_WRAPPERS
#define ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProjectFebGameModeBase(); \
	friend struct Z_Construct_UClass_AProjectFebGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AProjectFebGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectFeb"), NO_API) \
	DECLARE_SERIALIZER(AProjectFebGameModeBase)


#define ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAProjectFebGameModeBase(); \
	friend struct Z_Construct_UClass_AProjectFebGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AProjectFebGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectFeb"), NO_API) \
	DECLARE_SERIALIZER(AProjectFebGameModeBase)


#define ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectFebGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectFebGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectFebGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectFebGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectFebGameModeBase(AProjectFebGameModeBase&&); \
	NO_API AProjectFebGameModeBase(const AProjectFebGameModeBase&); \
public:


#define ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectFebGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectFebGameModeBase(AProjectFebGameModeBase&&); \
	NO_API AProjectFebGameModeBase(const AProjectFebGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectFebGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectFebGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectFebGameModeBase)


#define ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_12_PROLOG
#define ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_SPARSE_DATA \
	ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_RPC_WRAPPERS \
	ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_INCLASS \
	ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_SPARSE_DATA \
	ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROJECTFEB_API UClass* StaticClass<class AProjectFebGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectFeb_Source_ProjectFeb_ProjectFebGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
